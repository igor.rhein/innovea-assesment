# About Repository

This is a simple repo containing both frontend and backend for assessment. It has basically 3 functionalities: Authentication (create account/login/logout), listings for both the user's current search as well as his/her all time search history!

# Step By Step:
## Attention! Docker and docker compose required!
```
1. First things first, after project is cloned, get into backend folder (`cd backend` in case you are in the project's root folder)! 
2. Create a new .env file in backend folder and copy .env.example as is,
3. Careful! Docker and docker-compose are required!
3.1 Use the command docker-compose up, you may use the '-d' flag if you wish,
3.2 If required, you may also use the flag '--build' in order to force image to rebuild 
4. After docker-compose finishes up it's magic, run `npm run migrate` to set up database tables! 
5. You can create a new account through the api itself, however you may also seed a default user directly into database using the command 'npm run seed', which generates a user with email 'user@example.com' and password '1234',
6. Finally, you can use 'npm start' to start node server!
7. Now, with the backend running all shiny, you can set up the frontend!
7.1 It goes the same as before, only easier still: you just need to get into frontend folder and run docker-compose up! 
8. There you go! App will be running in localhost:3001 ready for use!
```

The documentation for API's routes were made using swagger and may be accesses via route 'localhost:3000/docs'

And thus we wrap up this quite simple set up

Hope you enjoy it, cheers!
