import { Button, Grid, TextField, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import SendIcon from "@mui/icons-material/Send";
import api from "../api";
import CustomizedSnackbars from "./Snackbar";
import NavBar from "./Navbar";

export default function HomePage() {
  const navigate = useNavigate();
  const [searchParameter, setSearchParameter] = useState("");
  const [snackbarMsg, setSnackbarMsg] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("");
  const localStorageUser = JSON.parse(localStorage.getItem("user"));

  async function handleSearchSubject() {
    const completeUrl = `${api().article}?filter=${searchParameter}`;
    const response = await fetch(completeUrl, {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${localStorageUser.jwt}`,
      },
    });

    if (response.status >= 400) {
      setSnackbarMsg("Falha ao realizar pesquisa sobre assunto!");
      setSnackbarSeverity("error");
      return;
    }

    const jsonResponse = await response.json();
    const { result } = jsonResponse;

    if (!result || result.length == 0) {
      setSnackbarMsg("Pesquisa retornou sem resultados!");
      setSnackbarSeverity("error");
      setSearchParameter("");
      return;
    }

    localStorage.setItem("articles", JSON.stringify(jsonResponse.result));
    setSnackbarMsg("Pesquisa realizada com sucesso! Seguem os resultados:");
    setSnackbarSeverity("success");

    navigate("/article");
  }

  useEffect(() => {
    const userFromLocalStorage = localStorage.getItem("user");

    if (!userFromLocalStorage) {
      navigate("/login");
    }
  }, []);

  return (
    <div>
      <NavBar />
      <Grid container direction="column" padding={5} maxWidth={"50%"}>
        <p>
          Use o campo abaixo para pesquisar online sobre qualquer tópico ou
          assunto nos artigos e notícias mais quentes, relevantes e populares da
          internet!
        </p>
        <TextField
          label="Tópico desejado"
          variant="standard"
          value={searchParameter}
          onChange={(e) => setSearchParameter(e.target.value)}
        />
        <Button
          onClick={handleSearchSubject}
          variant="outlined"
          endIcon={<SendIcon />}
          component="label"
          sx={{ p: 1.5, pl: 3, pr: 3, mt: 3, borderRadius: 6 }}
        >
          buscar!
        </Button>
        <CustomizedSnackbars
          errorMessage={snackbarMsg}
          errorSeverity={snackbarSeverity}
          onClose={() => setSnackbarMsg("")}
        />
      </Grid>
    </div>
  );
}
