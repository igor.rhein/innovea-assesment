import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";

export default function NavBar() {
  const userFromLocalStorage = localStorage.getItem("user");

  const navigation = [
    {
      name: "home",
      href: "/home",
      loginRequired: true,
    },
    {
      name: "home",
      href: "/",
      loginRequired: false,
    },
    {
      name: "create account",
      href: "/createAccount",
      loginRequired: false,
    },
    {
      name: "login",
      href: "/login",
      loginRequired: false,
    },
    {
      name: "view search history",
      href: "/searchHistory",
      loginRequired: true,
    },
    {
      name: "logout",
      href: "/",
      loginRequired: true,
    },
  ];

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ backgroundColor: "black" }}>
        <Toolbar>
          {navigation.map((item) => {
            if (userFromLocalStorage && item.name === "Logout") {
              return (
                <Button
                  type="button"
                  color="inherit"
                  size="large"
                  sx={{ mr: 2 }}
                  href={item.href}
                  style={{ color: "white" }}
                  onClick={() => {
                    localStorage.clear();
                  }}
                >
                  {item.name}
                </Button>
              );
            } else if (userFromLocalStorage && item.loginRequired) {
              return (
                <Button
                  color="inherit"
                  size="large"
                  sx={{ mr: 2 }}
                  style={{ color: "white" }}
                  href={item.href}
                >
                  {item.name}
                </Button>
              );
            } else if (!userFromLocalStorage && !item.loginRequired) {
              return (
                <Button
                  color="inherit"
                  size="large"
                  sx={{ mr: 2 }}
                  style={{ color: "white" }}
                  href={item.href}
                >
                  {item.name}
                </Button>
              );
            }
          })}
        </Toolbar>
      </AppBar>
    </Box>
  );
}
