import { Button, Grid, TextField } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import api from "../api";
import CustomizedSnackbars from "./Snackbar";
import NavBar from "./Navbar";

export default function Login() {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [snackbarMsg, setSnackbarMsg] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("");

  async function handleLogin() {
    const response = await fetch(api().login, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    });

    if (response.status >= 400) {
      setSnackbarMsg("Falha no login!");
      setSnackbarSeverity("error");
      return;
    }
    const jsonResponse = await response.json();

    localStorage.setItem("user", JSON.stringify(jsonResponse));
    navigate("/home");
  }

  return (
    <div>
      <NavBar />
      <Grid container direction="column" padding={8} gap={4}>
        <TextField
          label="E-mail"
          variant="standard"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          value={password}
          label="Senha"
          variant="standard"
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button onClick={handleLogin} variant="contained">
          Login
        </Button>
        <CustomizedSnackbars
          errorMessage={snackbarMsg}
          errorSeverity={snackbarSeverity}
          onClose={() => setSnackbarMsg("")}
        />
      </Grid>
    </div>
  );
}
