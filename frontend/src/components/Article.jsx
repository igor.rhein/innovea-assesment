import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import NavBar from "./Navbar";

export default function Article() {
  const [rows, setRows] = useState([]);

  const getArticles = async () => {
    return JSON.parse(localStorage.getItem("articles"));
  };

  useEffect(() => {
    const localStorageUser = localStorage.getItem("user");
    if (!localStorageUser) {
      navigate("/login");
    }

    const localArticles = localStorage.getItem("articles");
    if (!localArticles) {
      navigate("/home");
    }

    getArticles().then((result) => {
      setRows(result);
    });
  }, []);

  return (
    <div>
      <NavBar />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Título</TableCell>
              <TableCell align="left">Descrição</TableCell>
              <TableCell align="left">Autor</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="left">{row.title}</TableCell>
                <TableCell align="left">{row.description}</TableCell>
                <TableCell align="left">
                  {row.author || "Desconhecido"}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
