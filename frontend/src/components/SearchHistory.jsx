import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";
import NavBar from "./Navbar";
import api from "../api";

export default function SearchHistory() {
  const [rows, setRows] = useState([]);

  const getSearchHistory = async () => {
    const localStorageUser = localStorage.getItem("user");
    const jsonUser = JSON.parse(localStorageUser);

    const response = await fetch(api().searchHistory, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${jsonUser.jwt}`,
      },
    });

    const result = (await response.json()).result;
    return result.map((item) => {
      const localDate = new Date(item.createdAt).toLocaleDateString();
      const dateArray = localDate.split("/");
      const date =
        (Number(dateArray[0]) >= 10 ? dateArray[0] : "0" + dateArray[0]) +
        "/" +
        (Number(dateArray[1]) >= 10 ? dateArray[1] : "0" + dateArray[1]) +
        "/" +
        dateArray[2];

      const result = item.result.reduce((prev, curr) => {
        const searchResultSimplified = `[Título: "${curr.title}" - por ${
          curr.author || "Desconhecido"
        }] / `;
        prev.push([searchResultSimplified]);
        return prev;
      }, []);

      return {
        ...item,
        date,
        result,
      };
    });
  };

  useEffect(() => {
    const localStorageUser = localStorage.getItem("user");
    if (!localStorageUser) {
      navigate("/login");
    }

    getSearchHistory().then((result) => {
      setRows(result);
    });
  }, []);

  return (
    <div>
      <NavBar />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell align="left">Parâmetro Pesquisado</TableCell>
              <TableCell align="left">Resultado da Busca</TableCell>
              <TableCell align="left">Data</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="left">{row.id}</TableCell>
                <TableCell align="left">{row.parameter}</TableCell>
                <TableCell align="left">{row.result}</TableCell>
                <TableCell align="left">{row.date}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
