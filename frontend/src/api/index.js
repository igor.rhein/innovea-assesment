const api = () => {
  const url = {
    prod: "http://prod:port",
    dev: "http://localhost:3000",
    test: "http://stage:port",
  };

  const apiURL = url.dev;

  const routes = {
    createAccount: `${apiURL}/auth`,
    login: `${apiURL}/auth/login`,
    article: `${apiURL}/article`,
    searchHistory: `${apiURL}/article/history`,
  };

  return routes;
};

export default api;
