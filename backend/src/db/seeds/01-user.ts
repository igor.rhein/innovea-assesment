import { Knex } from 'knex';
import knexInstance from '..';
import { hash } from '../../encryption';

export async function seed(trx: Knex.Transaction): Promise<void> {
	await knexInstance.table('users').insert({
		email: 'user@example.com',
		enabled: true,
		passwordHash: await hash('1234'),
	});
}
