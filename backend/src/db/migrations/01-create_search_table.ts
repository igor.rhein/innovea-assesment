import { Knex } from 'knex';

export async function up(db: Knex): Promise<void> {
	await db.schema.createTable('searches', (table) => {
		table.uuid('id').primary().defaultTo(db.raw('uuid_generate_v4()'));
		table.uuid('userId').references('id').inTable('users');
		table.string('parameter').notNullable();
		table.json('result').notNullable();
		table.timestamp('createdAt', { useTz: false }).notNullable().defaultTo(db.fn.now());
		table.timestamp('updatedAt', { useTz: false }).notNullable().defaultTo(db.fn.now());
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable('searches');
}
