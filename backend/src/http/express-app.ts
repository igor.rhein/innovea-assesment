import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import routes from '../routes';
import logger from '../log/logger';
import { HttpError } from './error';
import { HttpStatus } from './status';
import config from '../config';

const app = express();

// Pug default locals
app.locals.config = config;

app.use(
	morgan(':remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" :response-time ms', {
		stream: {
			write: (msg) => logger.http(msg),
		},
	}),
);

if (config.environment === 'development') {
	app.use(cors({ maxAge: 36000 }));
}

if (config.environment === 'development') {
	Error.stackTraceLimit = Infinity;
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', routes);

app.use((err: Error, req: express.Request, res: express.Response, _: express.NextFunction) => {
	if (err instanceof HttpError) {
		res.status(err.statusCode);
		if (err.statusCode >= 400 && err.statusCode < 500) {
			logger.warn(err);
			if (err.content) {
				res.json(err.content);
			} else {
				res.send(err.message);
			}
		} else {
			logger.error(err);
			res.end();
		}
	} else {
		logger.error(err);
		res.status(HttpStatus.InternalServerError).end();
	}
});

app.use((_: express.Request, res: express.Response) => {
	res.status(HttpStatus.NotFound).render('404');
});

export default app;
