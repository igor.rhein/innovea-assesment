import { expect } from 'chai';
import sinon from 'sinon';
import axios from 'axios';
import { fetchArticles } from '../../services/article';

describe('fetchArticles', () => {
	let axiosGetStub: sinon.SinonStub;

	beforeEach(() => {
		axiosGetStub = sinon.stub(axios, 'get');
	});

	afterEach(() => {
		axiosGetStub.restore();
	});

	it('successfully fetches articles', async () => {
		const mockData = {
			articles: [
				{ title: 'Article 1', description: 'Description 1' },
				{ title: 'Article 2', description: 'Description 2' },
			],
		};
		axiosGetStub.resolves({ data: mockData });

		const filter = 'technology';
		const pageSize = 10;
		const page = 1;
		const result = await fetchArticles(filter, pageSize, page);

		expect(axiosGetStub.calledWithMatch(sinon.match(`https://newsapi.org/v2/everything?q=${filter}&apiKey=`))).to.be.true;
		expect(axiosGetStub.calledWithMatch(sinon.match(`&pageSize=${pageSize}&page=${page}&sortBy=relevancy`))).to.be.true;
		expect(result).to.deep.equal(mockData.articles);
	});

	it('handles API error', async () => {
		const errorMessage = 'API error';
		axiosGetStub.rejects(new Error(errorMessage));

		const filter = 'technology';
		const pageSize = 10;
		const page = 1;
		try {
			await fetchArticles(filter, pageSize, page);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal(errorMessage);
			expect(axiosGetStub.called).to.be.true;
		}
	});

	it('handles empty response', async () => {
		const mockData = {
			articles: [],
		};
		axiosGetStub.resolves({ data: mockData });

		const filter = 'technology';
		const pageSize = 10;
		const page = 1;
		const result = await fetchArticles(filter, pageSize, page);

		expect(result).to.deep.equal([]);
	});
});
