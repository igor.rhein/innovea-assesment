import { expect } from 'chai';
import bcrypt from 'bcryptjs';
import sinon from 'sinon';
import {
	create, del, User, update, get, UserSearchCols,
} from '../../services/user';
import knexInstance from '../../db/index';

describe('user creation', () => {
	let bcryptHashStub: sinon.SinonStub;
	let KnexInsertStub: sinon.SinonStub;

	beforeEach(() => {
		bcryptHashStub = sinon.stub(bcrypt, 'hash');
		KnexInsertStub = sinon.stub(knexInstance, 'insert');
	});

	afterEach(() => {
		bcryptHashStub.restore();
		KnexInsertStub.restore();
	});

	let user: User;
	it('creates and deletes a user', async () => {
		user = await create({
			email: 'hello@test.com',
			enabled: true,
			password: 'password',
		});
		expect(user).to.have.property('id');
		await del(user.id);
	});

	test('fails to create a user with incorrect password hashing', async () => {
		bcryptHashStub.rejects(new Error('Hashing failed'));

		const user = { email: 'test@example.com', password: 'password', enabled: true };
		try {
			await create(user);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Hashing failed');
			expect(bcryptHashStub.calledWith('password', 10)).to.be.true;
			expect(KnexInsertStub.called).to.be.false;
		}
	});

	test('fails to create a user with database insertion error', async () => {
		bcryptHashStub.resolves('hashed_password');
		KnexInsertStub.rejects(new Error('Database error'));

		const user = { email: 'test@example.com', password: 'password', enabled: true };
		try {
			await create(user);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Database error');
			expect(bcryptHashStub.calledWith('password', 10)).to.be.true;
			expect(
				KnexInsertStub.calledWith({
					email: 'test@example.com',
					passwordHash: 'hashed_password',
					enabled: true,
				}),
			).to.be.true;
		}
	});
});

describe('update', () => {
	let bcryptHashStub: sinon.SinonStub;
	let KnexUpdateStub: sinon.SinonStub;

	beforeEach(() => {
		bcryptHashStub = sinon.stub(bcrypt, 'hash');
		KnexUpdateStub = sinon.stub(knexInstance, 'update');
	});

	afterEach(() => {
		bcryptHashStub.restore();
		KnexUpdateStub.restore();
	});

	it('successfully updates a user', async () => {
		const userId = '123';
		bcryptHashStub.resolves('hashed_password');
		KnexUpdateStub.resolves();

		const user = { email: 'test@example.com', password: 'password', enabled: true };
		const result = await update(userId, user);

		expect(bcryptHashStub.calledWith('password', 10)).to.be.true;
		expect(
			KnexUpdateStub.calledWith(userId, {
				email: 'test@example.com',
				passwordHash: 'hashed_password',
				enabled: true,
				updatedAt: sinon.match.instanceOf(Date),
			}),
		).to.be.true;
		expect(result).to.deep.equal({
			userId,
			email: 'test@example.com',
			passwordHash: 'hashed_password',
			enabled: true,
		});
	});

	it('fails to update a user with incorrect password hashing', async () => {
		const userId = '123';
		bcryptHashStub.rejects(new Error('Hashing failed'));

		const user = { email: 'test@example.com', password: 'password', enabled: true };
		try {
			await update(userId, user);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Hashing failed');
			expect(bcryptHashStub.calledWith('password', 10)).to.be.true;
			expect(KnexUpdateStub.called).to.be.false;
		}
	});

	it('fails to update a user with database update error', async () => {
		const userId = '123';
		bcryptHashStub.resolves('hashed_password');
		KnexUpdateStub.rejects(new Error('Database error'));

		const user = { email: 'test@example.com', password: 'password', enabled: true };
		try {
			await update(userId, user);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Database error');
			expect(bcryptHashStub.calledWith('password', 10)).to.be.true;
			expect(
				KnexUpdateStub.calledWith(userId, {
					email: 'test@example.com',
					passwordHash: 'hashed_password',
					enabled: true,
					updatedAt: sinon.match.instanceOf(Date),
				}),
			).to.be.true;
		}
	});
});

describe('get', () => {
	let KnexSelectStub: sinon.SinonStub;

	beforeEach(() => {
		KnexSelectStub = sinon.stub(knexInstance, 'select');
	});

	afterEach(() => {
		KnexSelectStub.restore();
	});

	it('successfully retrieves a user', async () => {
		const user = {
			id: '123',
			email: 'test@example.com',
			passwordHash: 'hashed_password',
			enabled: true,
		};
		KnexSelectStub.resolves(user);

		const result = await get(UserSearchCols.email, 'test@example.com');

		expect(KnexSelectStub.calledWith({ email: 'test@example.com' })).to.be.true;
		expect(result).to.deep.equal(user);
	});

	it('fails to retrieve a user when not found', async () => {
		KnexSelectStub.rejects(null);

		try {
			await get(UserSearchCols.email, 'test@example.com');
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('User not found');
			expect(KnexSelectStub.calledWith({ email: 'test@example.com' })).to.be.true;
		}
	});
});

describe('del', () => {
	let KnexDeleteStub: sinon.SinonStub;

	beforeEach(() => {
		KnexDeleteStub = sinon.stub(knexInstance, 'delete');
	});

	afterEach(() => {
		KnexDeleteStub.restore();
	});

	it('successfully deletes a user', async () => {
		const userId = '123';
		KnexDeleteStub.resolves();

		await del(userId);

		expect(KnexDeleteStub.calledWith(userId)).to.be.true;
	});

	it('fails to retrieve a user when not found', async () => {
		KnexDeleteStub.rejects(new Error('User not found!'));

		try {
			await get(UserSearchCols.email, 'test@example.com');
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('User not found');
			expect(KnexDeleteStub.calledWith({ email: 'test@example.com' })).to.be.true;
		}
	});

	it('fails to delete a user with database deletion error', async () => {
		const userId = '123';
		KnexDeleteStub.rejects(new Error('Database error'));

		try {
			await del(userId);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Database error');
			expect(KnexDeleteStub.calledWith(userId)).to.be.true;
		}
	});
});
