import { expect } from 'chai';
import sinon from 'sinon';
import knexInstance from '../../db/index';
import { SearchCols, create, getMany } from '../../services/search'; // Replace 'yourModule' with the actual module name

describe('create', () => {
	let knexInstanceInsertStub: sinon.SinonStub;

	beforeEach(() => {
		knexInstanceInsertStub = sinon.stub(knexInstance, 'insert');
	});

	afterEach(() => {
		knexInstanceInsertStub.restore();
	});

	it('successfully creates a search', async () => {
		const search = {
			userId: '123',
			parameter: 'test',
			result: 'search result',
		};
		knexInstanceInsertStub.resolves();

		await create(search);

		expect(
			knexInstanceInsertStub.calledWith({
				userId: '123',
				parameter: 'test',
				result: JSON.stringify({ data: 'search result' }),
			}),
		).to.be.true;
	});

	it('fails to create a search with database insertion error', async () => {
		const search = {
			userId: '123',
			parameter: 'test',
			result: 'search result',
		};
		knexInstanceInsertStub.rejects(new Error('Database error'));

		try {
			await create(search);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Database error');
			expect(
				knexInstanceInsertStub.calledWith({
					userId: '123',
					parameter: 'test',
					result: JSON.stringify({ data: 'search result' }),
				}),
			).to.be.true;
		}
	});

	it('fails to create search with empty parameter', async () => {
		const search = {
			userId: '123',
			parameter: '',
			result: '',
		};
		knexInstanceInsertStub.rejects('Parameter cant be null nor empty!');

		await create(search);

		try {
			await create(search);
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Parameter cant be null nor empty!');
		}
	});
});

describe('getMany', () => {
	let knexInstanceWhereStub: sinon.SinonStub;

	beforeEach(() => {
		knexInstanceWhereStub = sinon.stub(knexInstance, 'where');
	});

	afterEach(() => {
		knexInstanceWhereStub.restore();
	});

	it('successfully retrieves multiple searches', async () => {
		const mockData = [
			{
				id: '1',
				userId: '123',
				parameter: 'test1',
				result: '{}',
				createdAt: '2023-05-31',
			},
			{
				id: '2',
				userId: '123',
				parameter: 'test2',
				result: '{}',
				createdAt: '2023-06-01',
			},
		];
		knexInstanceWhereStub.resolves(mockData);

		const result = await getMany(SearchCols.userId, '123');

		expect(knexInstanceWhereStub.calledWith({ userId: '123' })).to.be.true;
		expect(result).to.deep.equal(mockData);
	});

	it('handles no search results', async () => {
		const mockData: any[] = [];
		knexInstanceWhereStub.resolves(mockData);

		const result = await getMany(SearchCols.userId, '123');

		expect(knexInstanceWhereStub.calledWith({ userId: '123' })).to.be.true;
		expect(result).to.deep.equal([]);
	});

	it('handles database retrieval error', async () => {
		knexInstanceWhereStub.rejects(new Error('Database error'));

		try {
			await getMany(SearchCols.userId, '123');
			throw new Error('Test should have thrown an error');
		} catch (error) {
			expect(error.message).to.equal('Database error');
			expect(knexInstanceWhereStub.calledWith({ userId: '123' })).to.be.true;
		}
	});
});
