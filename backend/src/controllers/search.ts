import { Response } from 'express';
import * as searchService from '../services/search';

export async function getUserHistory(res: Response) {
	const { id } = res.locals.user;

	return searchService.getMany(searchService.SearchCols.userId, id);
}
