import { Request, Response } from 'express';
import * as articleService from '../services/article';
import * as searchService from '../services/search';

export async function fetchArticles(req: Request, res: Response) {
	const { filter, pageSize, page } = req.query;

	let requestedSize = Number(pageSize);
	let requestedPage = Number(page);

	if (!pageSize || Number(pageSize) > 100) {
		requestedSize = 100;
	}

	if (!page || Number(page) < 1) {
		requestedPage = 1;
	}

	const stringFilter = String(filter);
	const articles = await articleService.fetchArticles(stringFilter, requestedSize, requestedPage);

	await searchService.create({ userId: res.locals.user.id, parameter: stringFilter, result: articles });

	return articles;
}
