import * as userService from '../services/user';
import * as authService from '../services/auth';
import { CreateUser, SaveUser } from '../services/user';
import { HttpError } from '../http/error';
import { HttpStatus } from '../http/status';

export async function createUser(user: CreateUser) {
	const { email, password } = user;

	const alreadyExists = await userService.get(userService.UserSearchCols.email, email);

	if (alreadyExists) {
		throw new HttpError(HttpStatus.BadRequest, 'Invalid e-mail!');
	}

	const newUser: SaveUser = {
		...user,
		enabled: true,
	};
	await userService.create(newUser);

	return authService.login({ email, password });
}
