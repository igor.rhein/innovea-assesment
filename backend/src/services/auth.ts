import { transact } from '@cdellacqua/knex-transact';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {
	AuthResponse, table, cols, User,
} from './user';
import config from '../config';

export function createJwt(user: User): string {
	const token = jwt.sign({}, config.secret, {
		expiresIn: config.authentication.tokenExpirationSeconds,
		subject: user.id,
	});
	return token;
}

export function generateAuthResponse(user: User): AuthResponse {
	return {
		jwt: createJwt(user),
		user: {
			createdAt: user.createdAt,
			email: user.email,
			enabled: user.enabled,
			id: user.id,
			updatedAt: user.updatedAt,
		},
	};
}

export async function login({ email, password }: LoginParams): Promise<AuthResponse | null> {
	const user = await transact((db) => db(table)
		.where({ [cols.email]: email.toLowerCase(), [cols.enabled]: true })
		.first());
	if (!user) {
		return null;
	}
	const passwordMatches = await bcrypt.compare(password, user.passwordHash);
	if (!passwordMatches) {
		return null;
	}
	return generateAuthResponse(user);
}

export interface LoginParams {
	email: string;
	password: string;
}
