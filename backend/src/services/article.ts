import axios from 'axios';

const baseUrl = 'https://newsapi.org/v2/everything?q=';
const apiKey = `&apiKey=${process.env.NEWS_API_KEY}`;

export async function fetchArticles(filter: string, pageSize: number, page: number) {
	const { data } = await axios.get(`${baseUrl + filter + apiKey}&pageSize=${pageSize}&page=${page}&sortBy=relevancy`);

	return data.articles;
}
