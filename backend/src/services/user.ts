import bcrypt from 'bcryptjs';
import { Knex } from 'knex';
import { transact } from '@cdellacqua/knex-transact';

export const table = 'users';

export const cols = {
	id: 'id',
	email: 'email',
	passwordHash: 'passwordHash',
	enabled: 'enabled',
	createdAt: 'createdAt',
	updatedAt: 'updatedAt',
};

export function create(user: SaveUser, trx?: Knex.Transaction): Promise<User> {
	return transact(
		[
			async (db) => db(table).insert({
				[cols.email]: user.email.toLowerCase(),
				[cols.passwordHash]: await bcrypt.hash(user.password, 10),
				[cols.enabled]: user.enabled,
			}),
		],
		trx,
	);
}

export function update(id: string, user: Partial<SaveUser>, trx?: Knex.Transaction): Promise<User> {
	return transact(
		[
			async (db) => db(table)
				.where({ id })
				.update({
					[cols.email]: user.email?.toLowerCase(),
					[cols.passwordHash]: user.password && (await bcrypt.hash(user.password, 10)),
					[cols.enabled]: user.enabled,
					[cols.updatedAt]: new Date(),
				}),
		],
		trx,
	);
}

export function get(col: UserSearchCols, value: string): Promise<User> {
	return transact((db) => db(table)
		.where({ [col]: value })
		.first());
}

export function del(id: string, trx?: Knex.Transaction): Promise<void> {
	return transact(
		(db) => db(table)
			.where({ [cols.id]: id })
			.delete(),
		trx,
	);
}

export enum UserSearchCols {
	email = 'email',
	id = 'id',
}

export interface User {
	id: string;
	email: string;
	passwordHash: string;
	enabled: boolean;
	createdAt: Date;
	updatedAt: Date;
}

export interface CreateUser {
	email: string;
	password: string;
}
export interface SaveUser extends CreateUser {
	enabled: boolean;
}

export interface AuthResponse {
	jwt: string;
	user: Omit<User, 'passwordHash'>;
}
