import { Knex } from 'knex';
import { transact } from '@cdellacqua/knex-transact';

export const table = 'searches';

export const cols = {
	id: 'id',
	userId: 'userId',
	parameter: 'parameter',
	result: 'result',
	createdAt: 'createdAt',
	updatedAt: 'updatedAt',
};

export async function create(search: CreateSearch, trx?: Knex.Transaction) {
	return transact(
		[
			async (db) => db(table).insert({
				[cols.userId]: search.userId,
				[cols.parameter]: search.parameter,
				[cols.result]: JSON.stringify(search.result),
			}),
		],
		trx,
	);
}

export function getMany(col: SearchCols, value: string): Promise<Search[]> {
	return transact((db) => db(table)
		.where({ [col]: value })
		.orderBy('createdAt', 'desc'));
}

export interface Search {
	id: string;
	userId: string;
	result: object;
	createdAt: Date;
	updatedAt: Date;
}

export interface CreateSearch {
	userId: string;
	parameter: string;
	result: string;
}

export enum SearchCols {
	parameter = 'parameter',
	userId = 'userId',
}
