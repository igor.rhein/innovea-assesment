import { Router } from 'express';
import authRoutes from './auth';
import authenticatedRoutes from './authenticated';
import swaggerRoutes from '../docs/config';

const r: Router = Router();
export default r;

r.get('/hello-api', (_, res) => res.send('Hello World!'));

r.use('/auth', authRoutes);

r.use('/docs', swaggerRoutes);

r.use('/', authenticatedRoutes);
