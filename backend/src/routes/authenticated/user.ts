import { asyncWrapper } from '@cdellacqua/express-async-wrapper';
import { Router } from 'express';
import { HttpStatus } from '../../http/status';
import { del } from '../../services/user';
import { generateAuthResponse } from '../../services/auth';

const r: Router = Router();
export default r;

// Renew JWT
r.post(
	'/jwt',
	asyncWrapper(async (req, res) => {
		const authResponse = generateAuthResponse(res.locals.user);
		res.status(HttpStatus.Created).json(authResponse);
	}),
);

r.delete(
	'/',
	asyncWrapper(async (req, res) => {
		await del(res.locals.user.id);
		res.status(HttpStatus.NoContent).end();
	}),
);
