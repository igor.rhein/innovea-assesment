import { SerializableError } from '@cdellacqua/serializable-error';
import jwt from 'jsonwebtoken';
import { asyncWrapper } from '@cdellacqua/express-async-wrapper';
import { transact } from '@cdellacqua/knex-transact';
import { HttpError } from '../../http/error';
import config from '../../config';
import { HttpStatus } from '../../http/status';
import { table } from '../../services/user';

const middleware = asyncWrapper(async (req, res, next) => {
	if (!req.headers.authorization) {
		throw new HttpError(HttpStatus.Unauthorized, 'unauthorized', undefined);
	}
	let decoded: { sub: any; iat: any };
	try {
		decoded = jwt.verify(req.headers.authorization.substring('Bearer '.length), config.secret) as { sub: string; iat: number };
	} catch (err) {
		throw new HttpError(HttpStatus.Unauthorized, 'unauthorized', err);
	}
	if (!decoded) {
		throw new HttpError(HttpStatus.Unauthorized, 'unauthorized', new SerializableError('jwt decode returned a falsy value'));
	}
	const user = await transact((db) => db(table).where({ id: decoded.sub }).first());
	if (!user) {
		throw new HttpError(HttpStatus.Unauthorized, 'unauthorized', new SerializableError('jwt refers to a missing user'));
	}
	if (!user.enabled) {
		throw new HttpError(HttpStatus.Unauthorized, 'unauthorized', new SerializableError('jwt refers to a disabled user'));
	}

	res.locals.user = user;
	next();
});

export default middleware;
