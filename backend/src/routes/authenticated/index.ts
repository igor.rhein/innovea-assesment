import { Router } from 'express';
import userRoutes from './user';
import articleRoutes from './article';
import authMiddleware from './_middleware';
import searchRoutes from './search';

const r: Router = Router();
export default r;

r.use(authMiddleware);

r.use('/user', userRoutes);

r.use('/article', [articleRoutes, searchRoutes]);
