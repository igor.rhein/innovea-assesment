import { asyncWrapper } from '@cdellacqua/express-async-wrapper';
import { Router } from 'express';
import * as searchController from '../../controllers/search';
import { HttpStatus } from '../../http/status';

const r: Router = Router();
export default r;

r.get(
	'/history',
	asyncWrapper(async (req, res) => {
		const searches = await searchController.getUserHistory(res);
		res.status(HttpStatus.OK).send({ result: searches });
	}),
);
