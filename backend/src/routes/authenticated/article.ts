import { asyncWrapper } from '@cdellacqua/express-async-wrapper';
import { Router } from 'express';
import { query } from 'express-validator';
import * as articleController from '../../controllers/article';
import { HttpStatus } from '../../http/status';
import { validationMiddleware } from '../../http/validation';

const r: Router = Router();
export default r;

r.get(
	'/',
	[query('filter').notEmpty(), validationMiddleware()],
	asyncWrapper(async (req, res) => {
		const articles = await articleController.fetchArticles(req, res);
		res.status(HttpStatus.OK).send({ result: articles });
	}),
);
