import { Router } from 'express';
import { body } from 'express-validator';
import { asyncWrapper } from '@cdellacqua/express-async-wrapper';
import * as authService from '../services/auth';
import { validationMiddleware } from '../http/validation';
import { HttpStatus } from '../http/status';
import * as userController from '../controllers/user';

const r: Router = Router();
export default r;

const validation = [body('email').isEmail(), body('password').isString().isLength({ min: 4 }), validationMiddleware()];

r.post(
	'/',
	validation,
	asyncWrapper(async (req, res) => {
		const result = await userController.createUser({
			email: req.body.email,
			password: req.body.password,
		});
		if (!result) {
			res.status(HttpStatus.InternalServerError).end();
		} else {
			res.status(HttpStatus.OK).json({ message: 'User created!', result });
		}
	}),
);

r.post(
	'/login',
	validation,
	asyncWrapper(async (req, res) => {
		const loginResult = await authService.login({
			email: req.body.email,
			password: req.body.password,
		});
		if (!loginResult) {
			res.status(HttpStatus.Unauthorized).end();
		} else {
			res.status(HttpStatus.OK).json(loginResult);
		}
	}),
);
