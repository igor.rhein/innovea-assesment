/**
 * @swagger
 * components:
 *   securitySchemes:
 *     Bearer:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 */

/**
 * @swagger
 * tags:
 *   name: Auth
 * /auth:
 *   post:
 *     summary: Creates new account
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *               password:
 *                 type: string
 *                 format: password
 *             example:
 *               email: user@example.com
 *               password: '1234'
 *     responses:
 *       "200":
 *         description: OK
 *       "400":
 *         description: Bad request
 *
 * /auth/login:
 *   post:
 *     summary: Logs into application using a valid user data
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *               password:
 *                 type: string
 *                 format: password
 *             example:
 *               email: user@example.com
 *               password: '1234'
 *     responses:
 *       "200":
 *         description: OK
 *       "400":
 *         description: Bad request
 */

/**
 * @swagger
 * tags:
 *   name: Article
 *   description: Lists the latest articles
 *
 * /article:
 *   get:
 *     summary: Lists articles filtered by parameter
 *     tags: [Article]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - in: query
 *         name: filter
 *         schema:
 *           type: string
 *         description: Subject to search for
 *         required: true
 *     responses:
 *       "200":
 *         description: OK
 *       "400":
 *         description: Bad request
 *
 * /article/history:
 *   get:
 *    summary: Lists all searches made by user
 *    tags: [Article]
 *    security:
 *       - Bearer: []
 *    responses:
 *      "200":
 *        description: OK
 *      "400":
 *        description: Bad request
 */
