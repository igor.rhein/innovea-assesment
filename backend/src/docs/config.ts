import { Router } from 'express';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const r: Router = Router();
export default r;

const swaggerDefinition = {
	openapi: '3.0.0',
	info: {
		title: 'Great Stuff API docs',
		version: '0.0.1',
		description: "There's node, there's express, there's typescript, there's postgres and then there's routing documentation!'",
		license: {
			name: 'MIT',
			url: 'https://github.com/saisilinus/node-express-mongoose-typescript-boilerplate.git',
		},
	},
	servers: [
		{
			url: `http://localhost:${process.env.HTTP_PORT}/`,
			description: 'Development Server',
		},
	],
};

const specs = swaggerJsdoc({
	swaggerDefinition,
	apis: ['src/docs/swagger.ts'],
});

r.use('/', swaggerUi.serve);
r.get(
	'/',
	swaggerUi.setup(specs, {
		explorer: true,
	}),
);
