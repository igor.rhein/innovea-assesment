module.exports = {
	// require: ['dotenv/config', 'ts-node/register'],
	require: [
		"ts-node/register", 'dotenv/config'
	  ],
	  recursive: true,
	  extension: [
		"ts",
		"js",
		"tsx"
	  ],
};
